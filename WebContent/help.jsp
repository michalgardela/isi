<%@page language="Java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@include file="/WEB-INF/siteElements/Head.jsp"%>
<script type="text/javascript" src="js/rozwijanie.js"></script>

</head>
<body>

	<%@include file="/WEB-INF/siteElements/HeaderMenu.jsp"%>

	<div class="content">
		<section id="contentText"> 
		<!-- ZAWATOŚĆ STRONY  -->

		<dl id="pytanie1">
			<dt>
				<font class="rozlista">1. Jak mogę złożyć zamówienie?</font>
			</dt>
			<dd>
				Zamówienia można złożyć na trzy sposoby :</br> - odwiedzając osobiście
				siedzibę firmy w Krakowie.</br> - poprzez złożenie dyspozycji na naszej
				infolinii (12 628 20 01) </br> - za pośrednictwem serwisu www, po
				wcześniejszej rejestracji lub zalogowaniu.

			</dd>
		</dl>
		<script type="text/javascript">
			//          
			new Menu('pytanie1');
			//
		</script>

		<dl id="pytanie2">
			<dt>
				<font class="rozlista">2. Jak obliczane są koszty wypożyczeń?</font>
			</dt>
			<dd>
				Każdy samochód ma przypisana do siebie stawkę za dobę. Przy
				dłuższych okresach wypożyczeni można skorzystać z rabatów.
				Wypożyczający przed odbiorem pojazdu płaci za okres zdeklarowany
				przy składaniu zamówienia, kaucje w wysokości 500 zł, opcjonalnie
				rozszerzone ubezpieczenie.</br>Przy zdaniu pojazdu, jeżeli nie został
				przekroczony termin oraz nie zostały wyrządzone żadne szkody
				zwracana jest kaucja.

			</dd>
		</dl>
		<script type="text/javascript">
			//          
			new Menu('pytanie2');
			//
		</script>
	</div>
	
	<!-- KONIEC ZAWATOŚCI  -->
	</section>
	</div>

	<%@include file="/WEB-INF/siteElements/Footer.jsp"%>

</body>

</html>