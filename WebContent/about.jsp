<%@page language="Java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@include file="/WEB-INF/siteElements/Head.jsp"%>

</head>

<body>

	<%@include file="/WEB-INF/siteElements/HeaderMenu.jsp"%>

	<div class="content">
		<section id="contentText">
		 <!-- ZAWATOŚĆ STRONY  -->

			<img src="images/about/cars.jpg" alt="Tekst alternatywny"
				align="left" />OrderAndDrive - nasza firma działa w Krakowie na
			rynku wypożyczalni samochodów od wielu lat. Oferuje swoim klientom
			wygodne samochody. Wszystkie wyposażone w klimatyzację, elektryczne
			szyby, elektryczne lusterka, poduszki powietrzne i wiele innych
			udogodnień. Flota samochodów, którą posiadamy jest nowa ponieważ
			dbamy o bezpieczeństwo naszych klientów dlatego też co kilka lat
			wymieniamy samochody na nowe. Flota jaką posiadamy to samochody
			światowych marek , które swoją marką świadczą o renomie naszej firmy.
			Wygoda i bezpieczeństwo klienta jest dla nas najważniejsze. </br> </br>
			Samochody serwisowane są w autoryzowanych serwisach. Oferujemy naszym
			klientom wynajem samochodu, podstawiamy samochody w wybrane miejsce
			przez klienta, jesteśmy do dyspozycji 24 h na dobę. Proponujemy
			naszym klientom również skorzystania z naszej oferty w przypadku
			potrzeby samochodu zastępczego. Po zakończeniu wynajmu wystawiamy
			fakturę VAT, którą klient przedstawia ubezpieczalni i otrzymuje zwrot
			kosztów wynajmu. </br> </br> Zachęcamy wszystkie firmy do skorzystania z
			możliwości wynajmu długoterminowego. Jest to bardzo wygodna oferta
			dla klienta ponieważ to w naszym obowiązku jest serwisowanie
			samochodu, ubezpieczenie go, wymiana opon itp. Klient nie musi
			martwić się co w przypadku awarii samochodu ponieważ otrzyma od nas
			samochód zastępczy. </br> </br>
			<center>
				<h2>Zapraszamy do wspólpracy!</h2>
			</center>

		<!-- KONIEC ZAWATOŚCI  -->
		 </section>
	</div>

	<%@include file="/WEB-INF/siteElements/Footer.jsp"%>

</body>

</html>