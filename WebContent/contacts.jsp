<%@page language="Java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@include file="/WEB-INF/siteElements/Head.jsp"%>

</head>
<body>

	<%@include file="/WEB-INF/siteElements/HeaderMenu.jsp"%>

	<div class="content">
		<section id="contentText"> 
		<!-- ZAWATOŚĆ STRONY  -->

			<center>
				<h2>OrderAndDrive</h2>
				Warszawska 24</br> 
				30-962 Kraków</br> 
				tel.12 628 20 00</br> 
				email:info@orderanddrive.com </br> </br> </br> 
				
				Lokalizacja: </br>
				<iframe width="400" height="300" frameborder="0" scrolling="no"
					marginheight="0" marginwidth="0"
					src="https://maps.google.pl/maps?hl=pl&amp;q=krak%C3%B3w+warszawska+24&amp;ie=UTF8&amp;hq=&amp;hnear=Warszawska+24,+Krak%C3%B3w,+Wojew%C3%B3dztwo+ma%C5%82opolskie&amp;t=m&amp;z=14&amp;ll=50.071465,19.944043&amp;output=embed"></iframe>
				<br />
			</center>
		
		<!-- KONIEC ZAWATOŚCI  -->
		 </section>
	</div>

	<%@include file="/WEB-INF/siteElements/Footer.jsp"%>

</body>

</html>