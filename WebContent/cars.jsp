<%@page language="Java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@include file="/WEB-INF/siteElements/Head.jsp"%>
<%@ page import = "com.gruby.autobasemanager.*"%>
<%@ page import = "com.michal.*"%>

<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/Humanst521_BT_400.font.js"></script>
<script type="text/javascript" src="js/Humanst521_Lt_BT_400.font.js"></script>
<script type="text/javascript" src="js/roundabout.js"></script>
<script type="text/javascript" src="js/roundabout_shapes.js"></script>
<script type="text/javascript" src="js/gallery_init.js"></script>
<script type="text/javascript" src="js/cufon-replace.js"></script>
</head>
<body>

	<%@include file="/WEB-INF/siteElements/HeaderMenu.jsp"%>

	<div class="content">
		<section id="contentText"> 
		<!-- ZAWATOŚĆ STRONY  --> 
		
		<!-- GALERIA -->
		<center>
			<section id="gallery">
			<div class="container">
				<ul id="myRoundabout">
					<%

						Auto[] autoList = AutoBaseManager.getAutoList();
				
						for(Auto a : autoList) {
							
							out.print("<li><center>");
		        		    out.print("<img src=\""+a.getLink()+"\" alt=\"\" />"+"</br>"+"</br>");
		        		    out.print("<big>"+a.getMarka()+" "+a.getModel()+"</big>"+"</br>");
		        		    out.print("Ilość drzwi "+a.getIl_drzwi()+"</br>");
		        		    out.print("Typ silnika "+a.getTyp_silnika()+"</br>");
		        		    out.print("Moc "+a.getMoc()+"</br>");
		        		    out.print("Bagażnik "+a.getPoj_bag()+" L"+"</br>");
		        		    out.print("Kolor "+a.getKolor()+"</br>"+"</br>");
		        		    out.print("Cena "+a.getCena_dzien()+" zł/24h"+"</br>"+"</br>");
		        		    out.print("<a href=\"MainClass?page=rent&carID="+a.getID()+"\" \">Wypożycz</a>");
		        		    out.print("</center></li>");
						}
					%>
				</ul>
			</div>
			</section>
		</center>
		<!-- /GALERIA --> 
		
		<!-- KONIEC ZAWATOŚCI  -->
		 </section>
	</div>

	<%@include file="/WEB-INF/siteElements/Footer.jsp"%>

</body>

</html>
