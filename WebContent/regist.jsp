<%@page language="Java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@include file="/WEB-INF/siteElements/Head.jsp"%>
<%@ page import="com.gruby.autobasemanager.*"%>
<%@ page import="com.michal.*"%>

</head>

<body>

	<%@include file="/WEB-INF/siteElements/HeaderMenu.jsp"%>
	<section id="contentText"> <!-- ZAWATOŚĆ STRONY  -->
	<div class="content">
		<h1>Rejestracja</h1>
		<%
			User.update(request);
			boolean success = false;

			if (request.getParameter("tried") != null) {
				
				try {
					
					String login = Forms.parseString(
							request.getParameter("login"), 3, 19);
					
					String pass1 = Forms.parseString(
							request.getParameter("pass1"), 4, 19);
					
					String pass2 = Forms.parseString(
							request.getParameter("pass2"), 4, 19);

					if (!pass1.equals(pass2)) {
						throw new Exception("Podane hasła są różne.");
					}

					String firstName = Forms.parseString(
							request.getParameter("firstName"), 3, 19);
					
					String secondName = Forms.parseString(
							request.getParameter("secondName"), 3, 19);
					
					String pesel = Forms.parseString(
							request.getParameter("pesel"), 11, 11);
					
					try {
						Long.parseLong(pesel);
					} catch(Exception e) {
						throw new Exception("Niepoprawny pesel.");
					}

					success = AutoBaseManager.makeNewCustomer(firstName,
							secondName, pesel, "", login, pass1);
				} catch (Exception e) {
					out.print(e.getMessage());
					%>
						<h2>Błąd dodawania użytkownika!</h2>
					<%
				}
			}
			
			if (success) {
	
		%><h2>Pomyślnie dodano użytkownika!</h2><%
		
			} else {
		%>
	
		<div class="formBlockLeft">
	
			Login </br> </br> Hasło </br> </br> Powtórz hasło </br> </br> </br> Imię </br> </br> Nazwisko </br> </br> PESEL </br> </br> </br>
	
		</div>
	
		<div class="formBlockRight">
			<form action="regist.jsp" method="get">
				<input type="hidden" name="tried" value="true" /> <input
					type="text" name="login" value="${request.getParameter("login")}"/></br> </br> <input type="text" name="pass1"  value="${request.getParameter("pass1")}"/></br> </br>
				<input type="text" name="pass2" value="${request.getParameter("pass2")}"/></br> </br> </br> <input type="text"
					name="firstName" value="${request.getParameter("firstName")}"/></br> </br> <input type="text" name="secondName" value="${request.getParameter("secondName")}"/></br> </br> <input
					type="text" name="pesel" value="${request.getParameter("pesel")}"/></br> </br> </br> <input type="submit" align="middle"
					value="Wyslij" /></br> </br>
			</form>
		</div>
		
		<%
			}
		%>

	<!-- KONIEC ZAWATOŚCI  -->
	</section>
	</div>

	<%@include file="/WEB-INF/siteElements/Footer.jsp"%>

</body>

</html>

