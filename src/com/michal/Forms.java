package com.michal;

public class Forms {

	
	public static final String parseString (Object o, int minLenght, int maxLength) throws FormParseException {
		if(o instanceof String && o != null) {
			String s = (String) o;
			
			if(s.length() >= minLenght && s.length() <= maxLength) {
				return s;
			} else {
				throw new FormParseException("Długość Stringa poza zakresem.");
			}
		} else {
			throw new FormParseException("Zmienna nie jest Stringiem.");
		}
	}
	
}
