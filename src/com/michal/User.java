package com.michal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class User {

	static HttpSession session = null;
	static boolean loggedIn = false;
	static boolean initialized = false;
	static String userType;
	
	public static class UserClassNotInitializedException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		
		public UserClassNotInitializedException() {
			super("Access to User without initialization!");
		}
		
	}
	
	private static void isInitialized() {
		if(!initialized) {
			throw new UserClassNotInitializedException();
		}
	}

	public static void update(HttpServletRequest request) {

		session = request.getSession(true);
		// Session exists
		if (session != null) {
			// Session was created in previous requests
			if (!session.isNew()) {
				// Get logged in attribute
				Object l = session.getAttribute("loggedIn");
				if (l != null) {
					loggedIn = (boolean) (l);
				}
			}
		}
		
		initialized = true;
	}

	public static HttpSession getSession() {
		isInitialized();
		return session;
	}

	public static boolean isLoggedIn() {
		isInitialized();
		return loggedIn;
	}

	public static boolean logIn(String login, String pass) {
		isInitialized();
		
		// TODO get user type from db
		if (login != null && pass != null && login.equals("admin")
				&& pass.equals("admin")) {
			loggedIn = true;
			session.setAttribute("loggedIn", true);
			return true;
		}

		return false;
	}

	public static void logOut() {
		isInitialized();
		session.setAttribute("loggedIn", false);
		loggedIn = false;
	}
	
	
	public String getUserType() {
		isInitialized();
		return userType;
	}
}
