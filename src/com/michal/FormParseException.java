package com.michal;

public class FormParseException extends Exception {
	
	FormParseException(String msg) {
		super(msg);
	}
}
