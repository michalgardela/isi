package com.gruby.autobasemanager;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;


public class AutoBaseManager 
{	
	
	{
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private static Connection c = null;
	
	/** metoda do nawiazywania polaczenia (tu zmieniamy wartosci zwiazane z konfiguracja polaczenia)
	 * 
	 * @return boolean oznaczajacy czy udalo sie polaczyc
	 */
	private static boolean connect()
	{
		try 
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			System.out.println("Uda�o sie zainicjowa� driver");
			c = DriverManager.getConnection(
					"jdbc:mysql://localhost/wypozyczalnia", "root", "");

			System.out.println ("Uda�o si� po��czy� z baz� danych...");
			
			return true;
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println ("Problem z driverem bazy danych...");
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{
			System.out.println ("Nie mo�na po��czy� si� z baza danych...");
			e.printStackTrace();
		} 
		catch (Exception e) 
		{
			System.out.println ("B��d przy po��czeniu z baz� danych...");
			e.printStackTrace();
		}
		
		return false;
	}
	
	/** metoda do rozlaczania polaczenia
	 * 
	 * @return boolean z wartoscia oznaczajaca czy poprawnie sie rozlaczylo
	 */
	private static boolean disconnect()
	{
		if (c != null) 
		{
			try 
			{
				c.close();
				System.out.println ("Po��czenie z baz� danych zamkni�te...");
				return true;
			} 
			catch (Exception e) 
			{
				System.out.println ("B��d przy zamykaniu bazy danych...");
			}
		}
		
		return false;
	}

	/** metoda do wlasnych zapytan
	 * 
	 * @param str - String z zapytaniem
	 * @return tablica dwywymiarowa Stringow (pierwsz wymiar to wiersz, drugi to kolumna)
	 */
	public static String[][] request(String str)
	{
		if(connect())
		{
			String tab[][] = null;
			
			ResultSet rs = null;
			try 
			{
				Statement st = c.createStatement();
				rs = st.executeQuery(str);
				ResultSetMetaData meta = rs.getMetaData();
				
				//sprawdzanie wielkosci
				//ilosc wierszy
				rs.last();
				int rowCount = rs.getRow();
				rs.first();
				//ilosc kolumn
				int columnCount = rs.getMetaData().getColumnCount();
				
				//utworzenie tablicy stringow z danymi
				if(rowCount == 0)
					return null;
				
				tab = new String[rowCount+1][columnCount];
				
				//		!!!pierwszy wiersz zawiera nazwy kolumn!!!
				for(int i = 1; i < columnCount + 1; i++)
				{
					tab[0][i - 1] = rs.getMetaData().getColumnName(i);
				}
				
				int i = 1;
				do
				{
					for(int col = 1; col < columnCount + 1; col++)
						tab[i][col - 1] = rs.getString(col);
						
					i++;
				}
				while (rs.next());
				
				rs.close();
				st.close();				
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
				return null;
			}
			
			disconnect();
			return tab;
		}
		else
			return null;
	}
	
	/** metoda do tworzenia nowego pracownika
	 * 
	 * @param firstName
	 * @param secondName
	 * @param PESEL
	 * @param sex
	 * @param position
	 * @param isWorking
	 * @param login
	 * @param password
	 * @return 
	 */
	public static boolean makeNewEmployee(String firstName, String secondName, String PESEL, String sex, String position, boolean isWorking, String login, String password)
	{
		if(connect())
		{
			try 
			{
				c.setAutoCommit(false);
				
				Statement st = c.createStatement();
				st.executeUpdate("INSERT INTO pracownik (imie, nazwisko, PESEL, plec, stanowisko, pracuje) VALUES ('" + firstName + "', '" 
						+ secondName + "', '" + PESEL + "', '" + sex + "', '" + position + "', '" + ((isWorking) ? "1" : "0") + "');");
				
				st.executeUpdate("INSERT INTO `logowanie` (`ID_pracownika`, `login`, `haslo`) " +
						"VALUES ((SELECT `ID` FROM `pracownik` WHERE `imie` = '" + firstName + "' AND `nazwisko` = '" + secondName + 
						"' AND `PESEL` = '" + PESEL + "' AND `plec` = '" + sex + "'), '" + login + "', '" + password + "');");
				
				c.commit();
				
				st.close();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda tworzaca nowe auto
	 * 
	 * @param regNumber
	 * @param color
	 * @param brand
	 * @param model
	 * @param power
	 * @param doorNr
	 * @param luggageCap
	 * @param engineType
	 * @param costPerDay
	 * @return
	 */
	public static boolean makeNewAuto(String regNumber, String color, String brand, String model, 
			int power, int doorNr, int luggageCap, String engineType, String link,  float costPerDay, StanAuta stan)
	{
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();
				st.executeUpdate("INSERT INTO `auto` (`nr_rej`, `kolor`, `marka`, `model`, `moc`, `il_drzwi`, `poj_bag`, " +
						"`typ_silnika`, `cena_dzien`, `stan`) VALUES " +
						"('" + regNumber + "', '" + color + "', '" + brand + "', '" + model + "', '" + power + "', '" + doorNr + 
						"', '" + luggageCap + "', '" + engineType + "', `link` = '" + link + "', '" + costPerDay + "', '" 
						+ ( (stan == StanAuta.DOSTEPNY)? "dostepny" : (stan == StanAuta.NIEDOSTEPNY)? "niedostepny" : "uszkodzony" ) + "');");
				
				st.close();
				disconnect();
				return true;
			}
			catch(SQLException e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda do edycji auta
	 * 
	 * @param ID - nr id auta do edycji
	 * @param regNumber
	 * @param color
	 * @param brand
	 * @param model
	 * @param power
	 * @param doorNr
	 * @param luggageCap
	 * @param engineType
	 * @param costPerDay
	 * @return
	 */
	public static boolean editAuto(int ID, String regNumber, String color, String brand, String model, 
			int power, int doorNr, int luggageCap, String engineType, String link, float costPerDay, StanAuta stan)
	{
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();
				st.executeUpdate("UPDATE `auto` SET `nr_rej` = '" + regNumber + "', `kolor` = '" + color + 
						"', `marka` = '" + brand + "', `model` = '" + model + "', `moc` = '" + power + 
						"', `il_drzwi` = '" + doorNr + "', `poj_bag` = '" + luggageCap + 
						"', `typ_silnika` = '" + engineType + "', `cena_dzien` = '" + costPerDay + "', `link` = '" + link + 
						"', `stan` = '" + ( (stan == StanAuta.DOSTEPNY)? "dostepny" : ( (stan == StanAuta.NIEDOSTEPNY)? "niedostepny" : "uszkodzony" ) ) + 
						"' WHERE `ID` = '" + ID + "' ;");
				
				st.close();
				disconnect();
				return true;
			}
			catch(SQLException e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda ustawiajaca auto o podanym ID jako niedostepne
	 * 
	 * @param ID - int z ID pojazdu
	 * @return
	 */
	public static boolean deleteAuto(int ID)
	{
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();
				st.executeUpdate("UPDATE `auto` SET `stan` = 'niedostepny' WHERE `ID` = '" + ID + "' ;");
				
				st.close();
				disconnect();
				return true;
			}
			catch(SQLException e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda tworzy nowego klienta
	 * 
	 * @param firstName
	 * @param secondName
	 * @param PESEL
	 * @param sex
	 * @param login
	 * @param password
	 * @return
	 */
	public static boolean makeNewCustomer (String firstName, String secondName, String PESEL, String sex, String login, String password)
	{
		if(connect())
		{
			try 
			{
				c.setAutoCommit(false);
				
				Statement st = c.createStatement();
				st.executeUpdate("INSERT INTO klient (imie, nazwisko, PESEL, plec) VALUES ('" + firstName + "', '" 
						+ secondName + "', '" + PESEL + "', '" + sex + "');");
				
				st.executeUpdate("INSERT INTO `logowanie` (`ID_klienta`, `login`, `haslo`) " +
						"VALUES ((SELECT `ID` FROM `klient` WHERE `imie` = '" + firstName + "' AND `nazwisko` = '" + secondName + 
						"' AND `PESEL` = '" + PESEL + "' AND `plec` = '" + sex + "'), '" + login + "', '" + password + "');");
				
				c.commit();
				st.close();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda do wypozyczania (rezerwacji) auta
	 * 
	 * @param idAuta
	 * @param idKlienta
	 * @param dOd
	 * @param dDo
	 * @param oplata
	 * @return
	 */
	public static boolean reserveCar (long idAuta, long idKlienta, Date dOd, Date dDo, float oplata)
	{
		if(connect())
		{
			ResultSet rs = null;
			try 
			{
				c.setAutoCommit(false);
				
				Statement st = c.createStatement();
				
				rs = st.executeQuery("SELECT `stan` FROM `auto` WHERE `ID` = '" + idAuta + "';");
				
				if(rs.next() && !((String)rs.getString(1)).equals("dostepny"))
					return false;
				
				rs = st.executeQuery("SELECT `od`, `do` FROM `wypozyczenie` WHERE `ID_auta` = '" + idAuta + "';");
				
				while(rs.next())
				{
					Date d1 = rs.getDate("od");
					Date d2 = rs.getDate("do");
					
					//jezeli data "od" zawiera sie miedzy zajetymi terminami zwracamy false
					if(dOd.compareTo(d1) >= 0 && dOd.compareTo(d2) <= 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
					
					//jezeli data "do" ...
					if(dDo.compareTo(d1) >= 0 && dDo.compareTo(d2) <= 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
					
					//jezeli data "od" jest przed dana rezerwacja a "do" po to zwracamy false
					if(dOd.compareTo(d1) < 0 && dDo.compareTo(d2) > 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
				}
				
				//jezeli zaden termin nie zwrocil false znaczy ze termin przez nas wybrany jest wolny
				st.executeUpdate("INSERT INTO wypozyczenie (ID_auta, ID_klienta, od, do, oplata, stan) VALUES " +
						"('" + idAuta + "', '" + idKlienta + "', '" + dOd.toString() + "', '" + dDo.toString() + 
						"', '" + oplata + "', 'rezerwacja');");
				
				st.close();
				rs.close();
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda do edycji wypozyczenia
	 * 
	 * @param ID
	 * @param idAuta
	 * @param idKlienta
	 * @param dOd
	 * @param dDo
	 * @param zwrocono
	 * @param oplata
	 * @param zaplacono
	 * @param idReklamacji
	 * @param stan
	 * @return
	 */
	public static boolean editCarRent (int ID, int idAuta, int idKlienta, Date dOd, Date dDo, 
			Date zwrocono, float oplata, float zaplacono, int idReklamacji, StanWypozyczenia stan)
	{
		if(connect())
		{
			ResultSet rs = null;
			try 
			{
				c.setAutoCommit(false);
				
				Statement st = c.createStatement();
				rs = st.executeQuery("SELECT `od`, `do` FROM `wypozyczenie` WHERE `ID_auta` = '" + idAuta + "';");
				
				while(rs.next())
				{
					Date d1 = rs.getDate("od");
					Date d2 = rs.getDate("do");
					
					//jezeli data "od" zawiera sie miedzy zajetymi terminami zwracamy false
					if(dOd.compareTo(d1) >= 0 && dOd.compareTo(d2) <= 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
					
					//jezeli data "do" ...
					if(dDo.compareTo(d1) >= 0 && dDo.compareTo(d2) <= 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
					
					//jezeli data "od" jest przed dana rezerwacja a "do" po to zwracamy false
					if(dOd.compareTo(d1) < 0 && dDo.compareTo(d2) > 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
				}
				
				//jezeli zaden termin nie zwrocil false znaczy ze termin przez nas wybrany jest wolny
				st.executeUpdate("UPDATE `wypozyczenie` SET `ID_auta` = '" + idAuta + "', `ID_klienta` = '" + idKlienta + 
						"', `od` = '" + dOd.toString() + "', `do` = '" + dDo.toString() + "', `zwrocono` = '" + zwrocono.toString() + 
						"', `oplata` = '" + oplata + "', `zaplacono` = '" + zaplacono + "', `ID_reklamacji` = '" + idReklamacji + 
						"', `stan` = '" + ( (stan == StanWypozyczenia.GWARANCJA)? "gwarancja" : ( (stan == StanWypozyczenia.REZERWACJA)? "rezerwacja" : "wypozyczenia" ) ) + "'" +
						" WHERE `ID` = '" + ID + "';");
				
				st.close();
				rs.close();
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda do edycji wypozyczenia
	 * 
	 * @param ID
	 * @param idAuta
	 * @param idKlienta
	 * @param dOd
	 * @param dDo
	 * @param oplata
	 * @param idReklamacji
	 * @param stan
	 * @return
	 */
	public static boolean editCarReserve (int ID, int idAuta, int idKlienta, Date dOd, Date dDo, 
			float oplata, StanWypozyczenia stan)
	{
		if(connect())
		{
			ResultSet rs = null;
			try 
			{
				c.setAutoCommit(false);
				
				Statement st = c.createStatement();
				rs = st.executeQuery("SELECT `od`, `do` FROM `wypozyczenie` WHERE `ID_auta` = '" + idAuta + "' AND `ID_klienta` != '" + idKlienta + "';");
				
				while(rs.next())
				{
					Date d1 = rs.getDate("od");
					Date d2 = rs.getDate("do");
					
					//jezeli data "od" zawiera sie miedzy zajetymi terminami zwracamy false
					if(dOd.compareTo(d1) >= 0 && dOd.compareTo(d2) <= 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
					
					//jezeli data "do" ...
					if(dDo.compareTo(d1) >= 0 && dDo.compareTo(d2) <= 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
					
					//jezeli data "od" jest przed dana rezerwacja a "do" po to zwracamy false
					if(dOd.compareTo(d1) < 0 && dDo.compareTo(d2) > 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
				}
				
				//jezeli zaden termin nie zwrocil false znaczy ze termin przez nas wybrany jest wolny
				st.executeUpdate("UPDATE `wypozyczenie` SET `ID_auta` = '" + idAuta + "', `ID_klienta` = '" + idKlienta + 
						"', `od` = '" + dOd.toString() + "', `do` = '" + dDo.toString() + 
						"', `oplata` = '" + oplata +  
						"', `stan` = '" + ( (stan == StanWypozyczenia.GWARANCJA)? "gwarancja" : ( (stan == StanWypozyczenia.REZERWACJA)? "rezerwacja" : "wypozyczenie" ) ) + "'" +
						" WHERE `ID` = '" + ID + "';");
				
				st.close();
				rs.close();
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda do wypozyczania auta
	 * 
	 * @param IDres
	 * @param date
	 * @param doplaty
	 * @return
	 */
	public static boolean rentCar(int IDres, Date date, float doplaty)
	{
		if(connect())
		{
			try 
			{
				c.setAutoCommit(false);
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `zaplacono` FROM `wypozyczenie` WHERE `ID`='" + IDres + "';");
				if(rs.next())
						doplaty = rs.getFloat(1) + doplaty;
				System.out.println(doplaty);
				
				st.executeUpdate("UPDATE `wypozyczenie` SET `od`='" + date + "', `zaplacono`='" + doplaty + "', `stan` = 'wypozyczenie' WHERE `ID`='" + IDres + "';");
				
				st.close();
				rs.close();
				
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					e.printStackTrace();
				}
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	 /** metoda do wypozyczania auta bez wplaty pieniedzy 
	 * @param IDres
	 * @param date
	 * @param doplaty
	 * @return
	 */
	public static boolean rentCar(int IDres, Date date)
	{
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				st.executeUpdate("UPDATE `wypozyczenie` SET `od`='" + date + "', `stan` = 'wypozyczenie' WHERE `ID`='" + IDres + "';");
				
				st.close();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
						
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda do zwracania auta
	 * 
	 * @param IDrent
	 * @param date
	 * @param doplaty
	 * @return
	 */
	public static boolean retCar (int IDrent, Date date, float doplaty)
	{
		if(connect())
		{
			try 
			{
				c.setAutoCommit(false);
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `zaplacono` FROM `wypozyczenie` WHERE `ID`='" + IDrent + "';");
				if(rs.next())
						doplaty = rs.getFloat(1) + doplaty;
				System.out.println(doplaty);
				
				st.executeUpdate("UPDATE `wypozyczenie` SET `do`='" + date + "', `zaplacono`='" + doplaty + "', WHERE `ID`='" + IDrent + "';");
				
				st.close();
				rs.close();
				
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** metoda do placenia
	 * 
	 * @param ID - id rezerwacji
	 * @param value
	 * @return
	 */
	public static boolean pay (long ID, float value)
	{
		if(connect())
		{
			try 
			{
				c.setAutoCommit(false);
				Statement st = c.createStatement();
				//sprawdzenie czy w zaplacono nie ma null w celu dodania poprawnie
				ResultSet rs = st.executeQuery("SELECT `zaplacono` FROM `wypozyczenie` WHERE `ID`='" + ID + "';");
				if(rs.next())
						value = rs.getFloat(1) + value;
				System.out.println(value);		
				
				st.executeUpdate("UPDATE `wypozyczenie` SET `zaplacono`='" + value + "' WHERE `ID`='" + ID + "';");
				
				st.close();
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** zlozenie reklamacji
	 * 
	 * @param IDrent
	 * @param desc
	 * @return
	 */
	public static boolean complain (long IDrent, String desc)
	{
		if(connect())
		{
			try 
			{
				c.setAutoCommit(false);
				Statement st = c.createStatement();	
				
				st.executeUpdate("INSERT INTO `reklamacja` (`opis`,`status`) VALUES ('" + desc + "','oczekuje na rozpatrzenie');");
				ResultSet rs = st.executeQuery("SELECT MAX(`ID`) FROM `reklamacja`;");
				rs.next();
				st.executeUpdate("UPDATE `wypozyczenie` SET `ID_reklamacji`=(SELECT MAX(`ID`) FROM `reklamacja`) WHERE `ID`='" + IDrent + "';");
				
				st.close();
				rs.close();
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** ustawienie statusu reklamacji wraz ze zmiana kosztow
	 * 
	 * @param IDrent
	 * @param status
	 * @param newCost
	 * @return
	 */
	public static boolean complainStatus (long IDrent, String status, float newCost)
	{
		if(connect())
		{
			try 
			{
				c.setAutoCommit(false);
				Statement st = c.createStatement();	
				
				st.executeUpdate("UPDATE `reklamacja` SET `status`='" + status + "' WHERE `ID`=(SELECT `ID_reklamacji`FROM `wypozyczenie` WHERE `ID`='" + IDrent + "');");
				st.executeUpdate("UPDATE `wypozyczenie` SET `oplata`='" + newCost + "' WHERE `ID` ='" + IDrent + "';");
				
				st.close();
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** zwrot jakiejs kwoty z wypozyczenia
	 * 
	 * @param IDrent
	 * @param cost
	 * @return
	 */
	public static boolean retCost (long IDrent, float cost)
	{
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				st.executeUpdate("UPDATE `wypozyczenie` SET `zaplacono`=`zaplacono` - '" + cost + "' WHERE `ID`='" + IDrent + "';");
				
				st.close();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	
	/** zwolnienie pracownika
	 * 
	 * @param IDempl
	 * @return
	 */
	public static boolean fire (long IDempl)
	{
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				st.executeUpdate("UPDATE `pracownik` SET `pracuje`='0' WHERE `ID` ='" + IDempl + "';");
				
				st.close();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** zmiana stganowiska
	 * 
	 * @param id
	 * @param stanowisko
	 * @return
	 */
	public static boolean changeEmplPos (long id, String stanowisko)
	{
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				st.executeUpdate("UPDATE `pracownik` SET `stanowisko`='" + stanowisko + "' WHERE `ID`='" + id + "';	");
				
				st.close();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
	
	/** logowanie
	 * 
	 * @param login
	 * @param password
	 * @return
	 */
	public static int login(String login, String password)
	{
		int id = -1;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID_klienta`, `ID_pracownika` FROM `logowanie` WHERE `login`='" + login + "' AND `haslo`='" + password + "';");
				if(rs.next())
				{
					int i = rs.getInt(1);
					if(i == 0)
						id = rs.getInt(2);
					else
						id = i;
				}
				System.out.println(id);
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return id;
	}
	
	public static Auto getAuto(int ID)
	{
		Auto auto = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `nr_rej`, `kolor`, `marka`, `model`, `moc`, " +
						"`il_drzwi`, `poj_bag`, `typ_silnika`, `link`, `cena_dzien`, `stan` " +
						"FROM `auto` WHERE `ID`='" + ID + "';");
				
				if(rs.next())
				{
					auto = new Auto(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6),
							rs.getInt(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getInt(11), 
							(rs.getString(12).equals("uszkodzony")) ? StanAuta.USZKODZONY : ((rs.getString(12).equals("niedostepny")) ? StanAuta.NIEDOSTEPNY : StanAuta.DOSTEPNY ));
				}
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return auto;
	}
	
	public static Klient getKlient(int ID)
	{
		Klient klient = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `imie`, `nazwisko`, `PESEL`, `plec` " +
						"FROM `klient` WHERE `ID`='" + ID + "';");
				
				if(rs.next())
				{
					klient = new Klient(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
				}
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return klient;
	}
	
	public static Pracownik getPracownik(int ID)
	{
		Pracownik pracownik = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `imie`, `nazwisko`, `PESEL`, `plec`, `stanowisko`, `pracuje`" +
						"FROM `pracownik` WHERE `ID`='" + ID + "';");
				
				if(rs.next())
				{
					pracownik = new Pracownik(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), 
							rs.getString(6), (rs.getInt(7) == 1) ? true : false);
				}
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return pracownik;
	}
	
	public static Wypozyczenie getWypozyczenie(int ID)
	{
		Wypozyczenie wypozyczenie = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `ID_Auta`, `ID_klienta`, `od`, `do`, `zwrocono`, `oplata`, `zaplacono`, `ID_reklamacji`, `stan`" +
						"FROM `wypozyczenie` WHERE `ID`='" + ID + "';");
				
				if(rs.next())
				{
					wypozyczenie = new Wypozyczenie(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDate(4), 
							rs.getDate(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), 
							( (rs.getString(9).equals("wypozyczenie")) ? StanWypozyczenia.WYPOZYCZENIE : (rs.getString(9).equals("gwarancja") ? StanWypozyczenia.GWARANCJA : StanWypozyczenia.REZERWACJA ) ) );
				}
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return wypozyczenie;
	}
	
	public static Reklamacja getReklamacja(int ID)
	{
		Reklamacja reklamacja = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `opis`, `status`" +
						"FROM `reklamacja` WHERE `ID`='" + ID + "';");
				
				if(rs.next())
				{
					reklamacja = new Reklamacja(rs.getInt(1), rs.getString(2), rs.getString(3));
				}
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return reklamacja;
	}
	
	public static Auto[] getAutoList()
	{
		Auto autoList[] = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `nr_rej`, `kolor`, `marka`, `model`, `moc`, " +
						"`il_drzwi`, `poj_bag`, `typ_silnika`, `link`, `cena_dzien`, `stan` " +
						"FROM `auto`;");
				
				rs.last();
				int rowCount = rs.getRow();
				rs.first();
				
				autoList = new Auto[rowCount];
				
				int i = 0;
				do
				{
					autoList[i] = new Auto(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6),
							rs.getInt(7), rs.getInt(8), rs.getString(9), rs.getString(10), rs.getInt(11), 
							(rs.getString(12).equals("uszkodzony")) ? StanAuta.USZKODZONY : ((rs.getString(12).equals("niedostepny")) ? StanAuta.NIEDOSTEPNY : StanAuta.DOSTEPNY ));
					
					i++;
				}
				while(rs.next());
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return autoList;
	}
	
	public static Klient[] getKlientList()
	{
		Klient klientList[] = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `imie`, `nazwisko`, `PESEL`, `plec` " +
						"FROM `klient`;");
				
				rs.last();
				int rowCount = rs.getRow();
				rs.first();
				
				klientList = new Klient[rowCount];
				
				int i = 0;
				do
				{
					klientList[i] = new Klient(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
					
					i++;
				}
				while(rs.next());
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return klientList;
	}
	
	public static Pracownik[] getPracownikList()
	{
		Pracownik pracownikList[] = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `imie`, `nazwisko`, `PESEL`, `plec`, `stanowisko`, `pracuje`" +
						"FROM `pracownik`;");
				
				
				rs.last();
				int rowCount = rs.getRow();
				rs.first();
				
				pracownikList = new Pracownik[rowCount];
				
				int i = 0;
				do
				{
					pracownikList[i] = new Pracownik(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), 
							rs.getString(6), (rs.getInt(7) == 1) ? true : false);
					
					i++;
				}
				while(rs.next());
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return pracownikList;
	}
	
	public static Wypozyczenie[] getWypozyczenieList()
	{
		Wypozyczenie wypozyczenieList[] = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `ID_Auta`, `ID_klienta`, `od`, `do`, `oplata`, `zaplacono`, `ID_reklamacji`, `stan`" +
						"FROM `wypozyczenie`");
				
				rs.last();
				int rowCount = rs.getRow();
				rs.first();
				
				wypozyczenieList = new Wypozyczenie[rowCount];
				
				int i = 0;
				do
				{
					wypozyczenieList[i] = new Wypozyczenie(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDate(4), 
							rs.getDate(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), 
							( (rs.getString(9).equals("wypozyczenie")) ? StanWypozyczenia.WYPOZYCZENIE : (rs.getString(9).equals("gwarancja") ? StanWypozyczenia.GWARANCJA : StanWypozyczenia.REZERWACJA ) ) );
					
					i++;
				}
				while(rs.next());
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return wypozyczenieList;
	}
	
	public static Reklamacja[] getReklamacjaList()
	{
		Reklamacja reklamacjaList[] = null;
		
		if(connect())
		{
			try 
			{
				Statement st = c.createStatement();	
				
				ResultSet rs = st.executeQuery("SELECT `ID`, `opis`, `status`" +
						"FROM `reklamacja`;");
				
				rs.last();
				int rowCount = rs.getRow();
				rs.first();
				
				reklamacjaList = new Reklamacja[rowCount];
				
				int i = 0;
				do
				{
					reklamacjaList[i] = new Reklamacja(rs.getInt(1), rs.getString(2), rs.getString(3));
					
					i++;
				}
				while(rs.next());
				
				st.close();
				rs.close();
				
				disconnect();
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				disconnect();
			}
		}
		
		return reklamacjaList;
	}
	
	/** metoda do kasowania uzytkownika
	 * 
	 * @param id
	 * @return
	 */
	public static boolean deleteCustomer(int id)
	{
		if(connect())
		{
			ResultSet rs = null;
			try 
			{
				c.setAutoCommit(false);
				
				Statement st = c.createStatement();
				rs = st.executeQuery("SELECT `od`, `do` FROM `wypozyczenie` WHERE `ID_klienta` = '" + id + "';");
				
				Date now = new Date( (new java.util.Date()).getTime());
				
				while(rs.next())
				{
					Date date = rs.getDate("do");
					
					//jezeli aktualna data (now) jest mniejsza od jakiejkolwiek dat "do" (date)
					if(now.compareTo(date) <= 0)
					{
						st.close();
						rs.close();
						c.rollback();
						disconnect();
						return false;
					}
				}
				
				//jezeli zaden termin nie zwrocil false znaczy ze termin przez nas wybrany jest wolny
				st.executeUpdate("DELETE FROM `klient` WHERE `ID` = '" + id + "';");
				
				st.close();
				rs.close();
				c.commit();
				
				disconnect();
				return true;
			}
			catch(Exception e)
			{
				System.out.println ("B��d przy pobieraniu danych...");
				e.printStackTrace();
				
				try 
				{
					c.rollback();
				} 
				catch (SQLException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				disconnect();
				return false;
			}
		}
		else
			return false;
	}
}