package com.gruby.autobasemanager;

public class Auto
{
	private int ID;
	private String nr_rej = null;
	private String kolor = null;
	private String marka = null;
	private String model = null;
	private int moc;
	private int il_drzwi;
	private int poj_bag;
	private String typ_silnika = null;
	private String link = null;
	private int cena_dzien;
	private StanAuta stan = null;
	
	public Auto(int iD, String nr_rej, String kolor, String marka,
			String model, int moc, int il_drzwi, int poj_bag,
			String typ_silnika, String link, int cena_dzien, StanAuta stan) 
	{
		super();
		ID = iD;
		this.nr_rej = nr_rej;
		this.kolor = kolor;
		this.marka = marka;
		this.model = model;
		this.moc = moc;
		this.il_drzwi = il_drzwi;
		this.poj_bag = poj_bag;
		this.typ_silnika = typ_silnika;
		this.cena_dzien = cena_dzien;
		this.stan = stan;
		this.link = link;
	}
	
	public StanAuta getStan()
	{
		return this.stan;
	}
	
	public int getID() {
		return ID;
	}

	public String getNr_rej() {
		return nr_rej;
	}

	public String getKolor() {
		return kolor;
	}

	public String getMarka() {
		return marka;
	}

	public String getModel() {
		return model;
	}

	public int getMoc() {
		return moc;
	}

	public int getIl_drzwi() {
		return il_drzwi;
	}

	public int getPoj_bag() {
		return poj_bag;
	}

	public String getTyp_silnika() {
		return typ_silnika;
	}
	
	public int getCena_dzien() {
		return cena_dzien;
	}
	
	public String getLink()
	{
		return this.link;
	}
}
