package com.gruby.autobasemanager;

public class Klient
{
	private int ID;
	private String imie = null;
	private String nazwisko = null;
	private String PESEL = null;
	private String plec = null;
	
	public Klient(int iD, String imie, String nazwisko, String pESEL,
			String plec) 
	{
		super();
		ID = iD;
		this.imie = imie;
		this.nazwisko = nazwisko;
		PESEL = pESEL;
		this.plec = plec;
	}

	public int getID() {
		return ID;
	}

	public String getImie() {
		return imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public String getPESEL() {
		return PESEL;
	}

	public String getPlec() {
		return plec;
	}
}