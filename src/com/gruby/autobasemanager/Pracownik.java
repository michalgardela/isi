package com.gruby.autobasemanager;

public class Pracownik
{
	private int ID;
	private String imie = null;
	private String nazwisko = null;
	private String PESEL = null;
	private String plec = null;
	private String stanowisko = null;
	private boolean pracuje;
	
	public Pracownik(int iD, String imie, String nazwisko, String pESEL,
			String plec, String stanowisko, boolean pracuje) {
		super();
		ID = iD;
		this.imie = imie;
		this.nazwisko = nazwisko;
		PESEL = pESEL;
		this.plec = plec;
		this.stanowisko = stanowisko;
		this.pracuje = pracuje;
	}

	public int getID() {
		return ID;
	}

	public String getImie() {
		return imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public String getPESEL() {
		return PESEL;
	}

	public String getPlec() {
		return plec;
	}

	public String getStanowisko() {
		return stanowisko;
	}

	public boolean isPracuje() {
		return pracuje;
	}
}
