package com.gruby.autobasemanager;

public class Reklamacja
{
	private int ID;
	private String opis = null;
	private String status = null;
	
	public Reklamacja(int ID, String opis, String status)
	{
		this.ID = ID;
		this.opis = opis;
		this.status = status;
	}
	
	public int getID()
	{
		return this.ID;
	}
	
	public String getOpis()
	{
		return this.opis;
	}
	
	public String getStatus()
	{
		return this.status;
	}
}