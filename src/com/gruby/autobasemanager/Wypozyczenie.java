package com.gruby.autobasemanager;

import java.sql.Date;

public class Wypozyczenie
{
	
	
	private int ID;
	private int ID_auta;
	private int ID_klienta;
	private Date data_od = null;
	private Date data_do = null;
	private int oplata;
	private int zaplacono;
	private int ID_reklamacji;
	private StanWypozyczenia stan = null;
	
	public Wypozyczenie(int iD, int iD_auta, int iD_klienta, Date data_od,
			Date data_do, int oplata, int zaplacono,
			int iD_reklamacji, StanWypozyczenia stan) 
	{
		super();
		ID = iD;
		ID_auta = iD_auta;
		ID_klienta = iD_klienta;
		this.data_od = data_od;
		this.data_do = data_do;
		this.oplata = oplata;
		this.zaplacono = zaplacono;
		ID_reklamacji = iD_reklamacji;
		this.stan = stan;
	}

	public int getID() {
		return ID;
	}

	public int getID_auta() {
		return ID_auta;
	}

	public int getID_klienta() {
		return ID_klienta;
	}

	public Date getData_od() {
		return data_od;
	}

	public Date getData_do() {
		return data_do;
	}

	public int getOplata() {
		return oplata;
	}

	public int getZaplacono() {
		return zaplacono;
	}

	public int getID_reklamacji() {
		return ID_reklamacji;
	}

	public StanWypozyczenia getStan() {
		return stan;
	}
}