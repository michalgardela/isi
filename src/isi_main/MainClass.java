package isi_main;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.michal.User;

/**
 * Servlet implementation class MainClass
 */
public class MainClass extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainClass() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("page").equals("rent"))
		{
			String idCar = request.getParameter("carID");
			System.out.println("IDCAR : "+idCar);
			User.update(request);
			if(!User.isLoggedIn())
				request.getRequestDispatcher("WEB-INF/rent.jsp?idCar="+idCar).forward(request, response);
			else
				request.getRequestDispatcher("/log.jsp").forward(request, response);
		}
		else if(request.getParameter("page").equals("userHistory"))
		{
			request.getRequestDispatcher("WEB-INF/userHistory.jsp").forward(request, response);
		}		
		else if(request.getParameter("page").equals("settings"))
		{
			request.getRequestDispatcher("WEB-INF/settings.jsp").forward(request, response);
		}		
		else if(request.getParameter("page").equals("logout"))
		{
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
