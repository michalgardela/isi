package isi_main;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;

import com.michal.User;

/**
 * Servlet implementation class LogIn
 */
public class LogIn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogIn() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session  = request.getSession();
		// TODO Auto-generated method stub
		/*
		 * PrintWriter out = response.getWriter(); String resultSite="",
		 * tresc=""; String paramName = request.getParameter("login"); String
		 * paramPass= request.getParameter("pass");
		 * 
		 * tresc = "Logowanie zakonczone sukcesem</br></br></br></br>"+
		 * "Użytkownik : "+paramName+"</br></br>"+ "Hasło : "+paramPass+"</br>";
		 * 
		 * UserPage user1 = new UserPage(tresc);
		 * 
		 * resultSite ="" + "<HTML>\n" + "<HEAD>" +
		 * "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />"
		 * + "<TITLE>Hello WWW</TITLE></HEAD>\n" + "<BODY>\n" +
		 * "Logowanie zakonczone sukcesem</br></br></br></br>"+
		 * "Użytkownik : "+paramName+"</br></br>"+ "Hasło : "+paramPass+"</br>"+
		 * "<br/><br/>"+ "</BODY></HTML>";
		 * request.getRequestDispatcher("/login.jsp").forward(request,
		 * response); //out.print(user1.toString());
		 */

		request.getRequestDispatcher("/WEB-INF/log.jsp").forward(request,
				response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		User.update(request);

		String username = request.getParameter("login");
		String password = request.getParameter("pass");

		System.out.println("Użytkownik: " + username + "\n");
		
		if(User.logIn(username, password)) {
			request.getRequestDispatcher("WEB-INF/user.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "Unknown user, please try again");
			response.sendRedirect("ISI_Project/home.jsp");
		};

//		if (username.equals("admin") && password.equals("admin")) {
//			// request.getSession().setAttribute("user", user);
//			System.out.println("Zalogowano");
//			HttpSession session  = request.getSession();
//			session.setAttribute("bZalogowano", true);
//			request.getRequestDispatcher("WEB-INF/user.jsp").forward(request,
//					response);
//		} else {
//			request.setAttribute("error", "Unknown user, please try again");
//			response.sendRedirect("ISI_Project/home.html");
//		}
	}

}
