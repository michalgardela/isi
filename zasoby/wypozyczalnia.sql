-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas wygenerowania: 08 Paź 2013, 21:21
-- Wersja serwera: 5.5.27
-- Wersja PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `wypozyczalnia`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `auto`
--

CREATE TABLE IF NOT EXISTS `auto` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `nr_rej` varchar(10) NOT NULL,
  `kolor` varchar(20) DEFAULT NULL,
  `marka` varchar(20) NOT NULL,
  `model` varchar(20) NOT NULL,
  `moc` int(10) DEFAULT NULL,
  `il_drzwi` int(10) DEFAULT NULL,
  `poj_bag` int(10) DEFAULT NULL,
  `typ_silnika` varchar(20) DEFAULT NULL,
  `link` varchar(200) NOT NULL,
  `cena_dzien` int(10) NOT NULL,
  `stan` enum('uszkodzony','niedostepny','dostepny') NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `nr_rej` (`nr_rej`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Zrzut danych tabeli `auto`
--

INSERT INTO `auto` (`ID`, `nr_rej`, `kolor`, `marka`, `model`, `moc`, `il_drzwi`, `poj_bag`, `typ_silnika`, `link`, `cena_dzien`, `stan`) VALUES
(1, 'KRA', 'zielony', 'VW', 'Golf', 75, 3, 300, '1.6', '', 250, 'uszkodzony'),
(2, 'KRA 1234', 'Fioletowy', 'FIAT', '500', 75, 3, 300, '1.3 TSI', '', 150, 'uszkodzony'),
(4, 'KRA 1235', 'Fioletowy', 'FIAT', '500', 75, 3, 300, '1.3 TSI', '', 150, 'uszkodzony'),
(6, 'KRA 2314', 'czerwony', 'Afa Romeo', 'Mito', 120, 3, 270, '1.4 TB LPG', '', 100, 'uszkodzony'),
(7, 'KR 12345', 'srebrny', 'Audi', 'A4', 140, 4, 480, '2.0 TDI', '', 150, 'uszkodzony'),
(8, 'KR 22114', 'bia?y', 'Audi', 'A3', 122, 3, 365, '1.4 TFSI', '', 120, 'uszkodzony'),
(9, 'KRA 2345', 'pomara?czowy', 'Audi', 'A1', 270, 5, 300, '1.2 TFSI', '', 100, 'uszkodzony'),
(10, 'KR 9083', 'niebieski', 'BMW', '320i', 184, 4, 390, '2.0 TSI', '', 150, 'uszkodzony'),
(11, 'KRA 8922', 'grafitowy', 'BMW', '528i', 245, 4, 520, '2.8 TSI', '', 200, 'uszkodzony'),
(12, 'KRA 7924', 'czarny', 'BMW', '23dX', 204, 5, 420, '2.0 TDI', '', 150, 'uszkodzony'),
(13, 'KR 67423', 'br?zowy', 'Opel', 'Corsa', 90, 5, 285, '1.3 CDTI', '', 80, 'uszkodzony'),
(14, 'KR 6721', 'czerwony', 'Chevrolet', 'Cruze', 140, 4, 450, '1.4 T DOHC', '', 80, 'uszkodzony'),
(15, 'KRA 4325', 'br?zowy', 'Dacia', 'Duster', 105, 5, 443, '1.6 16V 4x4', '', 80, 'niedostepny'),
(16, 'KR 48234', 'niebieski', 'Skoda', 'Fabia', 90, 5, 300, '1.6 TDI', '', 80, 'uszkodzony'),
(17, 'WRA23059', 'srebrny', 'honda', 'prelude', 210, 2, 310, '2.2 DOHC V-TEC', 'http://s1.blomedia.pl/autokult.pl/images/2010/07/Honda-Prelude-IV-4.jpg', 1000, 'dostepny');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE IF NOT EXISTS `klient` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `imie` varchar(20) NOT NULL,
  `nazwisko` varchar(20) NOT NULL,
  `PESEL` varchar(11) NOT NULL,
  `plec` varchar(10) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PESEL` (`PESEL`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`ID`, `imie`, `nazwisko`, `PESEL`, `plec`) VALUES
(1, 'Brian', 'Gaw?cki', '91000000000', 'm??czyzna'),
(2, 'Grzegorz', 'Chyb', '91021412033', 'mezczyzna');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `logowanie`
--

CREATE TABLE IF NOT EXISTS `logowanie` (
  `ID_klienta` int(10) DEFAULT NULL,
  `ID_pracownika` int(10) DEFAULT NULL,
  `login` varchar(20) NOT NULL,
  `haslo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `logowanie`
--

INSERT INTO `logowanie` (`ID_klienta`, `ID_pracownika`, `login`, `haslo`) VALUES
(1, NULL, 'brian', 'brian'),
(2, NULL, 'grzesiek', 'grzesiek'),
(NULL, 1, 'grzesiek', 'grzesiek'),
(6, NULL, 'gerard', 'gerard'),
(NULL, 5, 'geronimo', 'geronimo');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownik`
--

CREATE TABLE IF NOT EXISTS `pracownik` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `imie` varchar(20) NOT NULL,
  `nazwisko` varchar(20) NOT NULL,
  `PESEL` varchar(11) NOT NULL,
  `plec` varchar(10) NOT NULL,
  `stanowisko` varchar(20) NOT NULL,
  `pracuje` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PESEL` (`PESEL`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `pracownik`
--

INSERT INTO `pracownik` (`ID`, `imie`, `nazwisko`, `PESEL`, `plec`, `stanowisko`, `pracuje`) VALUES
(1, 'Grzegorz', 'Chyb', '12345678910', 'mezczyzna', 'menel', 0),
(5, 'Geronimo', 'Chyb', '1234567938', 'mezczyzna', 'szef', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `reklamacja`
--

CREATE TABLE IF NOT EXISTS `reklamacja` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `opis` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `reklamacja`
--

INSERT INTO `reklamacja` (`ID`, `opis`, `status`) VALUES
(4, 'Danuta do wymiany', 'Zostawiamy ja narazie'),
(5, 'Nie ma hamulcy', 'Jednak ma hamulce');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wypozyczenie`
--

CREATE TABLE IF NOT EXISTS `wypozyczenie` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `ID_auta` int(10) NOT NULL,
  `ID_klienta` int(10) NOT NULL,
  `od` date NOT NULL,
  `do` date NOT NULL,
  `oplata` decimal(10,0) NOT NULL,
  `zaplacono` decimal(10,0) DEFAULT NULL,
  `ID_reklamacji` int(10) DEFAULT NULL,
  `stan` enum('wypozyczenie','gwarancja','rezerwacja') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `wypozyczenie`
--

INSERT INTO `wypozyczenie` (`ID`, `ID_auta`, `ID_klienta`, `od`, `do`, `oplata`, `zaplacono`, `ID_reklamacji`, `stan`) VALUES
(1, 1, 1, '2013-10-01', '2013-10-08', 1000, NULL, 5, 'rezerwacja'),
(2, 1, 1, '3913-10-09', '3913-10-11', 1000, 500, NULL, 'wypozyczenie'),
(3, 1, 1, '1913-10-11', '1913-10-11', 2000, 2000, 4, 'wypozyczenie'),
(4, 1, 1, '2013-10-11', '2013-10-11', 1000, 1000, NULL, 'wypozyczenie'),
(5, 17, 6, '2013-10-05', '2013-10-07', 3000, NULL, NULL, 'wypozyczenie');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
